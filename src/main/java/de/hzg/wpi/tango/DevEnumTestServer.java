package de.hzg.wpi.tango;

import de.hzg.wpi.tango.aspects.SelfRegisteringTangoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.DeviceState;
import org.tango.server.ServerManager;
import org.tango.server.annotation.*;

/**
 * @author Igor Khokhriakov <igor.khokhriakov@hzg.de>
 * @since ${date}
 */
@SelfRegisteringTangoService(host = "hzgxenvtest")
@Device(transactionType = TransactionType.NONE)
public class DevEnumTestServer {
    private final Logger logger = LoggerFactory.getLogger(DevEnumTestServer.class);

    @State
    private DeviceState state;
    @Status
    private String status;
    @Attribute
    private TestType enumAttribute = TestType.VALUE1;

    public static void main(String[] args) {
        ServerManager.getInstance().start(args, DevEnumTestServer.class);
    }

    public TestType getEnumAttribute() {
        return enumAttribute;
    }

    public void setEnumAttribute(final TestType enumAttribute) {
        this.enumAttribute = enumAttribute;
    }

    @Init
    @StateMachine(endState = DeviceState.ON)
    public void init() throws Exception {
        logger.trace("init");
    }

    @Delete
    @StateMachine(endState = DeviceState.OFF)
    public void delete() throws Exception {
        logger.trace("delete");
    }

    public DeviceState getState() {
        logger.trace("state={}", state);
        return state;
    }

    public void setState(DeviceState state) {
        logger.trace("new state={}", state);
        this.state = state;
    }

    public String getStatus() {
        logger.trace("status={}", status);
        return status;
    }

    public void setStatus(String status) {
        logger.trace("new status={}", status);
        this.status = status;
    }

    public enum TestType {
        VALUE1, VALUE2
    }
}
